# Check source retrieval
import numpy

source = inspector.source(0)
same_source = inspector.source("proxy_sample1.log")

# Check column retrieval
column = source.column(1)
same_column = same_source.column("Axis 2")
assert numpy.array_equal(column, same_column)

# Check string column retrieval
string_column_name = "Axis 4"
string_column = source.column(string_column_name)
string_column_dict = source.column(string_column_name, inspector.string_as.dict)
string_column_id = source.column(string_column_name, inspector.string_as.id)
assert string_column_dict.size == 10
assert string_column_id.size == string_column.size
recomputed_string_column = numpy.empty(string_column.size, string_column.dtype)
for i in range(string_column.size):
    recomputed_string_column[i] = string_column_dict[string_column_id[i]]
assert numpy.array_equal(string_column, recomputed_string_column)

# Check selection retrieval
selection = source.selection()
for i in range(string_column.size):
    assert selection.get(i) % 2 == 1
sel_array = selection.get()
for i in range(sel_array.size):
    assert sel_array[i] % 2 == 1

# Check column insertion
new_column = column * 2
source.insert_column(new_column, "new column")

# Check layer insertion from current selection
layer_name = "new layer"
source.insert_layer(layer_name)
selection = source.selection(layer_name)
sel_array = selection.get()
for i in range(sel_array.size):
    assert sel_array[i] % 2 == 1

# Check layer insertion from bool array
layer_name = "new layer 2"
sel_array = numpy.fromfunction(lambda i: i % 10 == False, (source.row_count(),))
source.insert_layer(layer_name, sel_array)
selection = source.selection(layer_name)
sel_array_out = selection.get()
assert sel_array_out.size == sel_array.size
for i in range(sel_array_out.size):
    assert sel_array_out[i] == (i % 10 == False)
